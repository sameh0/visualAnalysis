<?php
ini_set('upload_max_filesize', '60M');     
#ini_set('max_execution_time', '999');
#ini_set('memory_limit', '128M');
ini_set('post_max_size', '60M'); 

if( !empty($_FILES['file']) && $_FILES['file']['error'] == 0) {

    $uploadDir = 'uploads/';
    
    if(!file_exists($uploadDir))
			mkdir($uploadDir, 0777, true);
		
     #Process image with GD library 
    $verifyimg = getimagesize($_FILES['file']['tmp_name']);

     #Make sure the MIME type is an image  
    $pattern = "#^(image/)[^\s\n<]+$#i";

    if (!preg_match($pattern, $verifyimg['mime'])){
        die("Only image files are allowed!");
    }
		 
    # Rename both the image and the extension  
    $uploadfile = $uploadDir.md5(basename( $_FILES["file"]["name"])).'.jpg';

    # Upload the file to a secure directory with the new name and extension */
    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
 				die(json_encode(array('file'=>md5(basename( $_FILES["file"]["name"])).'.jpg','state'=>"true")));
    } else {
        die("Image upload failed!");
    }
}
?> 
