//
//  ViewController.swift
//  visualAnalysis
//
//  Created by sameh on 5/21/17.
//  Copyright © 2017 Radvy. All rights reserved.
//

import UIKit
import VisualRecognitionV3
import Alamofire

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var imagePicker: UIImagePickerController!
    @IBOutlet weak var image: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func recogonition(url:String){
        // Do any additional setup after loading the view, typically from a nib.
        NSLog("button pressed")
        //NSLog("url: "+urlText.text!)
        let apiKey = " "
        let version = "2017-05-21" // use today's date for the most recent version
        let visualRecognition = VisualRecognition(apiKey: apiKey, version: version)
        
        let Url = URL(string: url)
        
        var status = ""
        let failure = { (error: NSError) in print(error) }
        /*
         visualRecognition.classify(image: self.urlText.text!){classifiedImages in
         print( "visual status ::::::::::::: " + (classifiedImages.images.description))
         }*/
        
        /*
         visualRecognition.classify(imageFile: url!){
         classifiedImages in
         print( "visual status ::::::::::::: " + (classifiedImages.images.description))
         //detecting classification
         if (!classifiedImages.images.isEmpty && !classifiedImages.images[0].classifiers.isEmpty &&
         !classifiedImages.images[0].classifiers[0].classes.isEmpty) {
         
         print( "######## classification : " + classifiedImages.images[0].classifiers[0].classes[0].classification)
         
         //detecting faces on the pictures with people
         if (!classifiedImages.images[0].classifiers[0].classes[0].classification.isEmpty &&  "people" == classifiedImages.images[0].classifiers[0].classes[0].classification) {
         print("##########  person found ###########")
         */
        
        visualRecognition.detectFaces(inImageFile: Url!,failure: failure as? ((Error) -> Void)) { imagesWithFaces in
            //visualRecognition.detectFaces(url, failure: failure) { //imagesWithFaces in
            //print(imagesWithFaces)
            
            print("HELLO \(imagesWithFaces)")
            if (!imagesWithFaces.images[0].faces.isEmpty) {
                var minAge = String()
                var maxAge = String()
                var gender = String()
                
                if imagesWithFaces.images[0].faces[0].age.min?.description != nil {
                    minAge = (imagesWithFaces.images[0].faces[0].age.min?.description)!
                }
                
                if imagesWithFaces.images[0].faces[0].age.max?.description != nil {
                    maxAge = (imagesWithFaces.images[0].faces[0].age.max?.description)!
                }
                gender = imagesWithFaces.images[0].faces[0].gender.gender
                
                
                print(status + " age min : " + minAge )
                print( status + "###### theperson's age max : " + maxAge )
                print("Gender" + gender)
                
                self.alert(
                    message: "Gender:" + gender + "\n Max Age:" + maxAge + "\n Min Age:" + minAge)
                self.spinner.stopAnimating()
                self.spinner.isHidden = true
                self.image.alpha = 1.0
            }
            // }
            // }
            //}
            //setting feedback on sentiment
            
            //      print("2 : " + status)
            
        }
        //setting feedback on sentiment
        //    print("1 : " + status)
        
    }
    
    @IBAction func analysisButtonPressed(_ sender: Any) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.cameraDevice = .front
        imagePicker.cameraViewTransform = imagePicker.cameraViewTransform.scaledBy(x: -1,y: 1);
        present(imagePicker, animated: true, completion: nil)
    }
    
    func upload(image: UIImage){
        
        let imgData = UIImageJPEGRepresentation(image, 0.2)!
        
        let parameters = ["name": "a.jpg"]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:"https://example.com/upload")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    if response.result.value != nil {
                        
                        let JSON = response.result.value as! NSDictionary
                        
                        if let fileName:String = JSON["file"] as? String
                        {
                            print(fileName)
                            self.recogonition(url: "https://example.com/upload" + fileName)
                        }
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        
        if let capturedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.spinner.startAnimating()
            self.image.alpha = 0.5
            image.image = capturedImage
            upload(image: capturedImage)
        }
        
        //image.image = info[UIImagePickerControllerOriginalImage] as? UIImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension UIViewController {
    
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

